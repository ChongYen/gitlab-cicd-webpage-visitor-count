# Define variables
variable "aws_region" {
  default = "ap-southeast-1"  
}

variable "ssh_key_name" {
  default = "aws-terraform"  
}

variable "subnet_cidr" {
  default = "192.168.0.0/16"  
}

# Create VPC
resource "aws_vpc" "main" {
  cidr_block = var.subnet_cidr
}

# Create subnets
resource "aws_subnet" "subnet1" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "192.168.1.0/24"  # First subnet
  availability_zone = "${var.aws_region}a"
}

resource "aws_subnet" "subnet2" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = "192.168.2.0/24"  # Second subnet
  availability_zone = "${var.aws_region}b"
}

# Create security group allowing HTTP traffic
resource "aws_security_group" "nginx_sg" {
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create EC2 instance
resource "aws_instance" "nginx_instance" {
  ami                    = "ami-0440d3b780d96b29d"  
  instance_type          = "t2.micro"  
  subnet_id              = aws_subnet.subnet1.id
  key_name               = var.ssh_key_name
#  key_path               = "./aws-terraform.pem"
  associate_public_ip_address = true
  security_groups        = [aws_security_group.nginx_sg.name]

  user_data = <<-EOF
              #!/bin/bash
              sudo yum update
              sudo yum install -y nginx
              sudo systemctl enable nginx
              sudo systemctl start nginx
			        echo "<h1>Sample Webserver" >> /var/www/html/index.html
              EOF

  tags = {
    Name = "nginx-instance"
  }
}

# Create load balancer
resource "aws_elb" "nginx_lb" {
  name               = "nginx-lb"
  availability_zones = ["${var.aws_region}a", "${var.aws_region}b"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    target              = "HTTP:80/"
    interval            = 30
    timeout             = 5
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  instances = [aws_instance.nginx_instance.id]

  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "nginx-lb"
  }
}
